from django.urls import path
from . import views
from .views import ShowMyTasksView

urlpatterns = [
    path("create/", views.create_task, name="create_task"),
    path("mine/", ShowMyTasksView.as_view(), name="show_my_tasks"),
]
