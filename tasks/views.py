from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    return render(request, "tasks/create_task.html", {"form": form})


class ShowMyTasksView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/show_my_tasks.html"
    context_object_name = "tasks"

    def get_queryset(self):
        user = self.request.user
        return Task.objects.filter(assignee=user)
