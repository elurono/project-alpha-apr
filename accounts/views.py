from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib.auth.models import User
from .forms import LoginForm, SignupForm


def login_view(request):
    if request.method == "GET":
        form = LoginForm()
        return render(request, "accounts/login.html", {"form": form})
    elif request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse("list_projects"))
        return render(
            request,
            "accounts/login.html",
            {"form": form, "error_message": "Invalid credentials"},
        )


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                error_message = "The passwords do not match"
                return render(
                    request,
                    "accounts/signup.html",
                    {"form": form, "error_message": error_message},
                )

            # Create a new user account
            user = User.objects.create_user(
                username=username, password=password
            )

            # Log in the user
            login(request, user)

            return redirect(reverse("list_projects"))
    else:
        form = SignupForm()

    return render(request, "accounts/signup.html", {"form": form})
