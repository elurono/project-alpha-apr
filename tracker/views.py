from django.shortcuts import redirect


def redirect_to_projects(request):
    return redirect("list_projects")
